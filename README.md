### B2C2 WebSocket Test

Requirements:

* Java 11+

Running:

* `./gradlew clean build`
* `java -jar build/libs b2c2-test-1.0.0.jar` or `java -jar build/libs b2c2-test-1.0.0.jar 8080` (custom port)

Usage:

* WS URL: `http://localhost:8888/ws`. Please, refer to instructions above to change default port.