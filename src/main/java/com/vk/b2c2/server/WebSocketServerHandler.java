package com.vk.b2c2.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vk.b2c2.client.ClientWebSocketHandler;
import com.vk.b2c2.client.WebSocketClient;
import com.vk.b2c2.model.Event;
import com.vk.b2c2.model.EventType;
import com.vk.b2c2.util.WsError;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.common.WebSocketSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.vk.b2c2.util.JsonUtils.*;
import static com.vk.b2c2.util.ValidationUtils.validateAuthHeader;
import static com.vk.b2c2.util.ValidationUtils.validateEvent;
import static com.vk.b2c2.util.WsError.*;
import static java.lang.Boolean.FALSE;
import static java.util.concurrent.ConcurrentHashMap.newKeySet;

public class WebSocketServerHandler extends WebSocketAdapter implements ClientWebSocketHandler {

    public final static String AUTH_HEADER_NAME = "Authorization";
    private final static Logger log = LoggerFactory.getLogger(WebSocketServerHandler.class);
    private final static int MAX_SESSIONS_PER_TOKEN = 2;

    // this should be moved to redis to work properly with multiple servers deployed
    private final Map<String, LinkedList<Session>> sessionsByToken = new ConcurrentHashMap<>();

    private final Map<String, WebSocketClient> sessionChannelMap = new ConcurrentHashMap<>();
    private final Map<String, Set<String>> subscribedInstrumentsBySession = new ConcurrentHashMap<>();

    @Override
    public void onWebSocketConnect(Session session) {
        super.onWebSocketConnect(session);
        try {
            String authorizationHeader = getAuthToken(session);
            WsError authError = validateAuthHeader(authorizationHeader);
            if (authError.getStatusCode() > 0) {
                sendErrorMessageAndClose(authError);
                return;
            }

            if (sessionChannelMap.get(getConnectionId(session)) != null) {
                sendErrorMessageAndClose(ERR_3006);
                return;
            }

            WebSocketClient webSocketClient = new WebSocketClient(this, session, authorizationHeader);
            sessionChannelMap.put(getConnectionId(session), webSocketClient);
            webSocketClient.open();

            // check sessions limit
            LinkedList<Session> sessions = sessionsByToken.computeIfAbsent(authorizationHeader, s -> new LinkedList<>());
            if (sessions.size() > MAX_SESSIONS_PER_TOKEN) {
                // close the 'oldest' session for this token
                sessions.removeFirst().close();
            }
            sessions.add(session);

            log.info("CONNECTED: connection {}, token {}", getConnectionId(session), authorizationHeader);
        } catch (Exception e) {
            sendErrorMessageAndClose(ERR_4000);
        }
    }

    @Override
    public void onWebSocketText(String message) {
        Event event;
        try {
            event = fromJson(message);
        } catch (JsonProcessingException e) {
            sendErrorMessage(new Event(ERR_3005.getStatusCode(), ERR_3005.getErrorMessage()));
            return;
        }

        event = validateEvent(event);
        if (!event.isSuccess()) {
            sendErrorMessage(event);
            return;
        }

        if (event.getEventType() == EventType.SUBSCRIBE) {
            Set<String> instruments = subscribedInstrumentsBySession.computeIfAbsent(getConnectionId(getSession()), session -> newKeySet());
            if (instruments.contains(event.getInstrument())) {
                Event errorEvent = new Event(event.getEventType(), FALSE, event.getTag(), ERR_3007.getStatusCode(),
                        ERR_3007.getErrorMessage(), null);
                sendErrorMessage(errorEvent);
                return;
            }
        }

        if (event.getEventType() == EventType.UNSUBSCRIBE) {
            Set<String> instruments = subscribedInstrumentsBySession.get(getConnectionId(getSession()));
            if (instruments == null || !instruments.contains(event.getInstrument())) {
                Event errorEvent = new Event(event.getEventType(), FALSE, event.getTag(), ERR_3008.getStatusCode(),
                        ERR_3008.getErrorMessage(), null);
                sendErrorMessage(errorEvent);
                return;
            }
        }

        try {
            sessionChannelMap.get(getConnectionId(getSession())).eval(message);
            log.debug("Sent message {}, connection {}, token {}.", message, getConnectionId(getSession()), getAuthToken(getSession()));
        } catch (IOException e) {
            log.debug("Sent error message {}, connection {}, token {}.", event, getConnectionId(getSession()), getAuthToken(getSession()));
        }
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        Session session = getSession();
        WsError error = WsError.fromCode(statusCode);
        log.info("DISCONNECT: connection {}, token {}, {}", getConnectionId(session), getAuthToken(session), error);
        removeAndCloseClientSession(session, error);
        removeServerSessionFromLocalState(session);
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        log.error("Caught generic exception", cause);
    }


    // error msg utils

    private void sendErrorMessage(Event event) {
        try {
            getSession().getRemote().sendString(toJson(event));
            log.debug("Sent error message {}, connection {}, token {}.", event, getConnectionId(getSession()), getAuthToken(getSession()));
        } catch (IOException e) {
            log.error("Sending error message {}, connection {}, token {} failed.",
                    event, getConnectionId(getSession()), getAuthToken(getSession()), e);
        }
    }

    private void sendErrorMessageAndClose(WsError error) {
        sendErrorMessageAndClose(getSession(), error);
    }

    private void sendErrorMessageAndClose(Session session, WsError error) {
        if (error == null) {
            error = ERR_4000;
        }
        if (session.isOpen()) {
            sendErrorMessage(new Event(error.getStatusCode(), error.getErrorMessage()));
        }
        session.close(error.getStatusCode(), error.getErrorMessage());
    }


    // local state cleanup

    private void removeAndCloseClientSession(Session session, WsError error) {
        WebSocketClient channel = sessionChannelMap.remove(getConnectionId(session));
        try {
            channel.close();
            log.info("DISCONNECT: client channel {}, connection {}, token {}",
                    channel.getId(), getConnectionId(session), getAuthToken(session));
        } catch (InterruptedException e) {
            log.error("DISCONNECT channel {}, connection {}, token {} failed.",
                    channel.getId(), getConnectionId(session), getAuthToken(session), e);
        }
    }

    // todo add auth filter and get from headers
    private void removeServerSessionFromLocalState(Session session) {
        String authHeader = getAuthToken(session);
        if (authHeader != null) {
            LinkedList<Session> sessionsByTokenList = sessionsByToken.get(authHeader);
            if (sessionsByTokenList != null) {
                sessionsByTokenList.remove(session);
            }
        }
        subscribedInstrumentsBySession.remove(getConnectionId(session));
    }

    @Override
    public void onMessage(Session session, String rawEvent) {
        try {
            boolean isSubscribeEvent = isSubscribeEvent(rawEvent);
            boolean isUnsubscribeEvent = isUnsubscribeEvent(rawEvent);
            if (!isSubscribeEvent && !isUnsubscribeEvent) {
                session.getRemote().sendString(rawEvent);
                return;
            }
            if (isSubscribeEvent) {
                if (isSuccessEvent(rawEvent)) {
                    subscribedInstrumentsBySession.computeIfAbsent(getConnectionId(getSession()), s -> newKeySet())
                            .add(parseInstrument(rawEvent));
                    log.info("UNSUBSCRIBE: connection {}, token {}, {}", getConnectionId(getSession()), getAuthToken(getSession()), rawEvent);
                }
            }
            if (isUnsubscribeEvent) {
                if (isSuccessEvent(rawEvent)) {
                    subscribedInstrumentsBySession.computeIfAbsent(getConnectionId(getSession()), s -> newKeySet())
                            .remove(parseInstrument(rawEvent));
                    log.info("SUBSCRIBE: connection {}, token {}, {}", getConnectionId(getSession()), getAuthToken(getSession()), rawEvent);
                }
            }
            session.getRemote().sendString(rawEvent);
        } catch (IOException e) {
            log.error("Can't process message {}", rawEvent, e);
        }
    }

    @Override
    public void onClose(Session session, WsError error) {
        sendErrorMessageAndClose(session, error);
    }

    // misc

    private String getAuthToken(Session session) {
        // todo add auth filter and get from headers
        return "Token e13e627c49705f83cbe7b60389ac411b6f86fee7";
    }

    private String getConnectionId(Session session) {
        return ((WebSocketSession) session).getConnection().getId();
    }
}
