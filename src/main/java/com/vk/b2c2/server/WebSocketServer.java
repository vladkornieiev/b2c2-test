package com.vk.b2c2.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketServer {

    private final static int DEFAULT_PORT = 8888;

    private final static Logger log = LoggerFactory.getLogger(WebSocketServer.class);

    public static void main(String[] args) {
        int port = DEFAULT_PORT;
        if (args.length > 0) {
            String portStr = args[0];
            port = Integer.parseInt(portStr);
        }
        Server server = new Server(new QueuedThreadPool(1000));
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port);
        connector.setReuseAddress(true);
        server.addConnector(connector);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder holderEvents = new ServletHolder("ws", ServerServlet.class);
        context.addServlet(holderEvents, "/ws");

        try {
            server.start();
            server.join();
        } catch (Throwable t) {
            log.error("Caught generic error", t);
        }
    }
}
