package com.vk.b2c2.client;

import com.vk.b2c2.util.WsError;
import io.netty.channel.*;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import org.eclipse.jetty.websocket.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class B2C2WebSocketClientHandler extends SimpleChannelInboundHandler<Object> {

    private final static Logger log = LoggerFactory.getLogger(B2C2WebSocketClientHandler.class);
    private final WebSocketClientHandshaker handshaker;
    private final Session session;
    private final ClientWebSocketHandler clientWebSocketHandler;
    private ChannelPromise handshakeFuture;

    public B2C2WebSocketClientHandler(WebSocketClientHandshaker handshaker, Session session, ClientWebSocketHandler clientWebSocketHandler) {
        this.handshaker = handshaker;
        this.session = session;
        this.clientWebSocketHandler = clientWebSocketHandler;
    }

    public ChannelFuture handshakeFuture() {
        return handshakeFuture;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        handshakeFuture = ctx.newPromise();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        handshaker.handshake(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.info("WebSocket Client disconnected!");
        clientWebSocketHandler.onClose(session, WsError.ERR_3000);
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, Object msg) {
        Channel ch = ctx.channel();
        if (!handshaker.isHandshakeComplete()) {
            try {
                handshaker.finishHandshake(ch, (FullHttpResponse) msg);
                log.info("WebSocket Client (channel {}) connected!", ch.id());
                handshakeFuture.setSuccess();
            } catch (WebSocketHandshakeException e) {
                log.info("WebSocket Client (channel {}) failed to connect", ch.id(), e);
                handshakeFuture.setFailure(e);
            }
            return;
        }

        if (msg instanceof FullHttpResponse) {
            FullHttpResponse response = (FullHttpResponse) msg;
            throw new IllegalStateException(
                    "Unexpected FullHttpResponse (getStatus=" + response.status() +
                            ", content=" + response.content().toString(CharsetUtil.UTF_8) + ')');
        }

        WebSocketFrame frame = (WebSocketFrame) msg;
        if (frame instanceof TextWebSocketFrame) {
            TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
            log.debug("WebSocket Client (channel {}) received message: {}", ch.id(), textFrame.text());
            clientWebSocketHandler.onMessage(session, textFrame.text());
        } else if (frame instanceof CloseWebSocketFrame) {
            CloseWebSocketFrame closeFrame = (CloseWebSocketFrame) frame;
            log.debug("WebSocket Client (channel {}) received closing frame - {}", ch.id(), closeFrame);
            clientWebSocketHandler.onClose(session, WsError.fromCode(closeFrame.statusCode()));
            ch.close();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("Caught generic exception", cause);
        if (!handshakeFuture.isDone()) {
            handshakeFuture.setFailure(cause);
        }
        clientWebSocketHandler.onClose(session, WsError.ERR_4000);
        ctx.close();
    }
}