package com.vk.b2c2.client;

import com.vk.b2c2.util.WsError;
import org.eclipse.jetty.websocket.api.Session;

public interface ClientWebSocketHandler {
    void onMessage(Session session, String rawEvent);

    void onClose(Session session, WsError error);
}
