package com.vk.b2c2.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vk.b2c2.model.Event;

public final class JsonUtils {
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final static String EVENT_SUBSCRIBE = "\"event\":\"subscribe\"";
    private final static String EVENT_UNSUBSCRIBE = "\"event\":\"unsubscribe\"";
    private final static String EVENT_SUCCESS = "\"success\":true";

    private JsonUtils() {

    }

    public static String parseInstrument(String str) throws JsonProcessingException {
        return MAPPER.readTree(str).get("instrument").asText();
    }

    public static boolean isSuccessEvent(String str) {
        return str.contains(EVENT_SUCCESS);
    }

    public static boolean isSubscribeEvent(String str) {
        return str.contains(EVENT_SUBSCRIBE);
    }

    public static boolean isUnsubscribeEvent(String str) {
        return str.contains(EVENT_UNSUBSCRIBE);
    }

    public static Event fromJson(String str) throws JsonProcessingException {
        return MAPPER.readValue(str, Event.class);
    }

    public static String toJson(Event event) throws JsonProcessingException {
        return MAPPER.writeValueAsString(event);
    }
}
