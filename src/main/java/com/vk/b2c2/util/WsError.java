package com.vk.b2c2.util;

public enum WsError {
    ERR_3000(3000, "Authentication failure."),
    ERR_3001(3001, "Authorization not in the headers"),
    ERR_3002(3002, "Endpoint does not exist – You are trying to access an endpoint that we don’t handle."),
    ERR_3003(3003, "Instrument is not allowed – The instrument you are trying to subscribe to is not in your tradable instruments."),
    ERR_3004(3004, "Subscription is invalid – The parameters for subscription are invalid. For details see Subscribing."),
    ERR_3005(3005, "Unable to Jsonise your message – The message you sent is not valid json."),
    ERR_3006(3006, "Already connected."),
    ERR_3007(3007, "Already subscribed – You are already subscribed to this instrument."),
    ERR_3008(3008, "Not subscribed yet – You are trying to unsubscribe to an instrument you are not subscribed."),
    ERR_3009(3009, "InvalidFormat – Message is not string."),
    ERR_3011(3011, "Invalid Message – There is something wrong with the message you sent (no event field…)."),
    ERR_3013(3013, "Connectivity issues – Usually triggered when no price has been received for a given instrument during more than two seconds. Usually resolved quickly."),
    ERR_3014(3014, "Unexpected error – We’re having connectivity issues. Please try later."),
    ERR_3015(3015, "Username changed – Your username changed on the platform, this will close the connection."),
    ERR_3016(3016, "Connectivity issues – Usually triggered by an issue with the pricer. Usually resolved quickly."),
    ERR_3017(3017, "Authorization header is malformed. It must be of the form Token te25a9eb2588cf022d84e02cdf08c7405fadd164."),
    ERR_3018(3018, "The max quantity for the instrument is been updated and the subscription for a level is not valid anymore. The connection will be closed."),
    ERR_3019(3019, "The given instrument doesn’t end with .SPOT or .CFD."),
    ERR_4000(4000, "Generic error – Unexpected error. Please reach out to the tech team."),
    ERR_NO_CODE(1000, null),
    NONE(0, null);

    private final String errorMessage;
    private final int statusCode;

    WsError(int statusCode, String errorMessage) {
        this.errorMessage = errorMessage;
        this.statusCode = statusCode;
    }

    public static WsError fromCode(int statusCode) {
        try {
            return WsError.valueOf("ERR_" + statusCode);
        } catch (IllegalArgumentException e) {
            return NONE;
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return "errorMessage='" + errorMessage + ", statusCode=" + statusCode;
    }
}
