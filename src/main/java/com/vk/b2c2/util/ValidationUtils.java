package com.vk.b2c2.util;

import com.vk.b2c2.model.Event;
import com.vk.b2c2.model.EventType;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;

import static com.vk.b2c2.util.WsError.*;
import static java.lang.Boolean.FALSE;
import static java.math.BigDecimal.valueOf;

public final class ValidationUtils {

    private final static Pattern AUTH_TOKEN_PATTERN = Pattern.compile("^Token\\s[a-z0-9]{40}$");

    private ValidationUtils() {
    }

    public static Event validateEvent(Event event) {
        if (event.getEventType() == EventType.SUBSCRIBE) {
            return validateSubscriptionEvent(event);
        }
        // no examples for other event types
        return event;
    }


    public static WsError validateAuthHeader(String authorizationHeader) {
        if (authorizationHeader == null) {
            return ERR_3001;
        }
        if (!AUTH_TOKEN_PATTERN.matcher(authorizationHeader).matches()) {
            return ERR_3017;
        }
        return NONE;
    }

    private static Event validateSubscriptionEvent(Event event) {
        Map<String, List<String>> errors = null;

        List<String> instrumentErrors = validateInstrument(event.getInstrument());
        if (instrumentErrors != null) {
            errors = new HashMap<>();
            errors.put("instrument", instrumentErrors);
        }

        List<String> levelErrors = validateLevels(event.getLevels());
        if (levelErrors != null) {
            errors = new HashMap<>();
            errors.put("levels", levelErrors);
        }

        if (errors != null) {
            return new Event(event.getEventType(), FALSE, event.getTag(), ERR_3004.getStatusCode(), ERR_3004.getErrorMessage(), errors);
        }
        return event;
    }

    private static List<String> validateLevels(List<BigDecimal> levels) {
        List<String> errors = null;
        if (levels == null) {
            errors = new ArrayList<>();
            errors.add("Length must be between 1 and 2.");
            return errors;
        }

        if (levels.size() > 2) {
            errors = new ArrayList<>();
            errors.add("Length must be between 1 and 2.");
        }

        if (levels.stream().anyMatch(level -> level.precision() > 4)) {
            if (errors == null) {
                errors = new ArrayList<>();
            }
            errors.add("Precision must be lower or equal to 4");
        }

        if (levels.stream().anyMatch(level -> level.compareTo(valueOf(0.1)) < 0 || level.compareTo(valueOf(500)) > 0)) {
            if (errors == null) {
                errors = new ArrayList<>();
            }
            errors.add("Level must be a number between 0.1 and 500 (included)");
        }

        return errors;
    }

    private static List<String> validateInstrument(String instrument) {
        List<String> errors = null;
        if (instrument == null || instrument.isBlank() || instrument.length() < 6) {
            errors = new ArrayList<>();
            errors.add("Length must be 6.");
        }
        if (instrument != null && !instrument.equals(instrument.toUpperCase(Locale.ENGLISH))) {
            if (errors == null) {
                errors = new ArrayList<>();
            }
            errors.add("Must be uppercase.");
        }
        return errors;
    }
}
