package com.vk.b2c2.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event {
    @JsonProperty(value = "event")
    private EventType eventType;
    private boolean success = true;
    private String tag;
    private String instrument;
    @JsonProperty(value = "error_message")
    private String errorMessage;
    @JsonProperty(value = "error_code")
    private Integer errorCode;
    private Map<String, List<String>> errors;
    private List<BigDecimal> levels;

    public Event() {
    }

    public Event(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.success = Boolean.FALSE;
        this.errorMessage = errorMessage;
    }

    public Event(EventType eventType, Boolean success, String tag, Integer errorCode,
                 String errorMessage, Map<String, List<String>> errors) {
        this.eventType = eventType;
        this.success = success;
        this.tag = tag;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errors = errors;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }

    public List<BigDecimal> getLevels() {
        return levels;
    }

    public void setLevels(List<BigDecimal> levels) {
        this.levels = levels;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", Event.class.getSimpleName() + "[", "]");
        if (eventType != null) {
            joiner.add("eventType=" + eventType);
        }
        joiner.add("success=" + success);
        if (tag != null) {
            joiner.add("tag='" + tag + "'");
        }
        if (instrument != null) {
            joiner.add("instrument='" + instrument + "'");
        }
        if (errorMessage != null) {
            joiner.add("errorMessage='" + errorMessage + "'");
        }
        if (errorMessage != null) {
            joiner.add("errorCode=" + errorCode);
        }
        if (errors != null) {
            joiner.add("errors=" + errors);
        }
        if (levels != null) {
            joiner.add("levels=" + levels);
        }

        return joiner.toString();
    }
}
