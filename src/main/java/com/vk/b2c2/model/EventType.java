package com.vk.b2c2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EventType {
    @JsonProperty("subscribe")
    SUBSCRIBE,
    @JsonProperty("unsubscribe")
    UNSUBSCRIBE,
    @JsonProperty("price")
    PRICE,
    @JsonProperty("tradable_instruments")
    TRADABLE_INSTRUMENTS,
    @JsonProperty("tradable_instruments_update")
    TRADABLE_INSTRUMENTS_UPDATE,
    @JsonProperty("username_update")
    USERNAME_UPDATE
}